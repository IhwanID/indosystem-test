# Football App

This Application is my task for Indosystem Test. All of data is from TheSportDB API https://www.thesportsdb.com/api.php

## Screenshots

<img src="https://gitlab.com/IhwanID/indosystem-test/raw/master/screenshoot/1.jpeg"
width="256">&nbsp;&nbsp;&nbsp;
<img src="https://gitlab.com/IhwanID/indosystem-test/raw/master/screenshoot/2.jpeg"
width="256">&nbsp;&nbsp;&nbsp;
<img src="https://gitlab.com/IhwanID/indosystem-test/raw/master/screenshoot/3.jpeg"
width="256">&nbsp;&nbsp;&nbsp;
<img src="https://gitlab.com/IhwanID/indosystem-test/raw/master/screenshoot/4.jpeg"
width="256">&nbsp;&nbsp;&nbsp;

### App Features

1. Match schedules based on choosen league.

 * [x] List of upcoming matches.
 * [x] List of last matches.
 * [x] Match search.
 * [x] Match Detail.

2. Teams information based on choosen league.

 * [x] List of teams.
 * [x] Team search.
 * [x] Team detail as well as player information detail.

3. Favorite Teams and Matches
 * [x] Insert and delete selected Matches to Database
 * [x] Insert and delete selected Teams to Database

4. App Test
 * [x] Unit Test
 * [x] Instrumentation Test


 ### Tech Stack

 * [x] Model View Presenter Approach
 * [x] Kotlin
 * [x] AsyncTask
 * [x] Anko
 * [x] Glide

 ## Author

* **Ihwan ID**
