package me.ihwan.footballappfinal.presenter

import com.google.gson.Gson
import me.ihwan.footballappfinal.TestContextProvider
import me.ihwan.footballappfinal.model.Event
import me.ihwan.footballappfinal.model.EventResponse
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.network.TheSportsDBApi
import me.ihwan.footballappfinal.view.EventView
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainPresenterTest {
    @Mock
    private
    lateinit var view: EventView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    private lateinit var presenter: EventPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter =EventPresenter(view, apiRepository, gson, TestContextProvider())
    }

    private val events: MutableList<Event> = mutableListOf()
    private val response = EventResponse(events)
    private val league = "4320"

    @Test
    fun testGetPastList(){

        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TheSportsDBApi.getPast(league)),
                EventResponse::class.java
        )).thenReturn(response)

        presenter.getPastEvent(league)

        Mockito.verify(view).showEvent(events)
    }

}