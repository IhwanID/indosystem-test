package me.ihwan.footballappfinal.presenter

import com.google.gson.Gson
import me.ihwan.footballappfinal.TestContextProvider
import me.ihwan.footballappfinal.model.Event
import me.ihwan.footballappfinal.model.EventResponse
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.network.TheSportsDBApi
import me.ihwan.footballappfinal.view.DetailEventView
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DetailEventPresenterTest {
    @Mock
    private
    lateinit var view: DetailEventView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    private lateinit var presenter: DetailEventPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter = DetailEventPresenter(view, apiRepository, gson, TestContextProvider())
    }

    private val events: MutableList<Event> = mutableListOf()
    private val response2 = EventResponse(events)
    private val teamId = "4321"


    @Test
    fun testGetDetail(){
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TheSportsDBApi.getDetailEvent(teamId)),
                EventResponse::class.java
        )).thenReturn(response2)

        presenter.getDetail(teamId)

        Mockito.verify(view).showEvent(events)
    }
}