package me.ihwan.footballappfinal

import kotlinx.coroutines.experimental.Unconfined
import me.ihwan.footballappfinal.util.CoroutineContextProvider
import kotlin.coroutines.experimental.CoroutineContext


class TestContextProvider: CoroutineContextProvider() {
    override val main: CoroutineContext = Unconfined
}