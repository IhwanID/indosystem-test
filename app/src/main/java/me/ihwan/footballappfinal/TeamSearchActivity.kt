package me.ihwan.footballappfinal

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.LinearLayout
import com.google.gson.Gson
import me.ihwan.footballappfinal.adapter.TeamAdapter
import me.ihwan.footballappfinal.model.Team
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.presenter.TeamPresenter
import me.ihwan.footballappfinal.view.TeamView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.themedAppBarLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView

class TeamSearchActivity : AppCompatActivity(), TeamView {

    private lateinit var query: String
    private lateinit var adapter: TeamAdapter
    private lateinit var listTeam: RecyclerView
    private lateinit var presenter: TeamPresenter
    private lateinit var toolbar: Toolbar

    private var teams: MutableList<Team> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        coordinatorLayout {
            lparams(width = matchParent, height = matchParent)

            fitsSystemWindows = true

            themedAppBarLayout(R.style.ThemeOverlay_AppCompat_Dark_ActionBar) {

                toolbar = themedToolbar {
                    R.style.ThemeOverlay_AppCompat_Light
                }.lparams(width = matchParent, height = wrapContent) {
                    scrollFlags = 0
                }

            }.lparams(width = matchParent, height = wrapContent)

            linearLayout {
                lparams(width = matchParent, height = matchParent)
                orientation = LinearLayout.VERTICAL

                relativeLayout{
                    lparams (width = matchParent, height = wrapContent)

                    listTeam = recyclerView {
                        lparams (width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }
                }

            }.lparams(width = matchParent, height = matchParent){
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }

        query = intent.getStringExtra("query")

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Search Team : $query"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = TeamAdapter(teams) { startActivity<TeamDetailActivity>(
                "team" to it.teamId) }

        listTeam.adapter = adapter



        val request = ApiRepository()
        val gson = Gson()

        presenter = TeamPresenter(this, request, gson)
        presenter.getSearchTeam(query)
    }

    override fun showTeam(data: List<Team>) {
        teams.clear()
        teams.addAll(data)
        adapter.notifyDataSetChanged()
    }

}
