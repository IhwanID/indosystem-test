package me.ihwan.footballappfinal.view

import me.ihwan.footballappfinal.model.Team

interface TeamView {
    fun showTeam(data: List<Team>)
}