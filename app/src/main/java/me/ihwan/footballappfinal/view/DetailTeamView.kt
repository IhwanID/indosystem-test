package me.ihwan.footballappfinal.view

import me.ihwan.footballappfinal.model.Team

interface DetailTeamView{
    fun showTeamDetail(data: List<Team>)
}