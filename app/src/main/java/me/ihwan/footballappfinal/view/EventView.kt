package me.ihwan.footballappfinal.view

import me.ihwan.footballappfinal.model.Event

interface EventView {
    fun showEvent(data: List<Event>)
}