package me.ihwan.footballappfinal.view

import me.ihwan.footballappfinal.model.Player

interface PlayerView{
    fun showPlayer(data: List<Player>?)
}