package me.ihwan.footballappfinal.view

import me.ihwan.footballappfinal.model.Event
import me.ihwan.footballappfinal.model.Team

interface DetailEventView {
    fun showTeam(dataHome: List<Team>, dataAway: List<Team>)
    fun showEvent(data: List<Event>)
}