package me.ihwan.footballappfinal

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.LinearLayout
import com.google.gson.Gson
import me.ihwan.footballappfinal.adapter.EventAdapter
import me.ihwan.footballappfinal.model.Event
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.presenter.EventPresenter
import me.ihwan.footballappfinal.view.EventView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.themedAppBarLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView

class EventSearchActivity : AppCompatActivity(), EventView {

    private lateinit var query: String
    private var events: MutableList<Event> = mutableListOf()
    private lateinit var adapter: EventAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var presenter: EventPresenter
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        coordinatorLayout {
            lparams(width = matchParent, height = matchParent)
            fitsSystemWindows = true

            themedAppBarLayout(R.style.ThemeOverlay_AppCompat_Dark_ActionBar) {
                toolbar = themedToolbar {
                    R.style.ThemeOverlay_AppCompat_Light
                    backgroundColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
                }.lparams(width = matchParent, height = wrapContent) {
                    scrollFlags = 0
                }
            }.lparams(width = matchParent, height = wrapContent)

            linearLayout {
                lparams(width = matchParent, height = matchParent)
                orientation = LinearLayout.VERTICAL

                listEvent = recyclerView {
                    id = R.id.eventSearchRecyclerView
                    layoutManager = LinearLayoutManager(ctx)
                }.lparams(width = matchParent, height = matchParent)

            }.lparams(width = matchParent, height = matchParent){
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }

        query = intent.getStringExtra("query")

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Match Of : $query"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = EventAdapter(events) { startActivity<EventDetailActivity>(
                "eventId" to it.eventId) }

        listEvent.adapter = adapter



        val request = ApiRepository()
        val gson = Gson()

        presenter = EventPresenter(this, request, gson)
        presenter.getSearchEvent(query)
    }

    override fun showEvent(data: List<Event>) {
        events.clear()
        events.addAll(data)
        adapter.notifyDataSetChanged()
    }

}
