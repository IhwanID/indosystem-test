package me.ihwan.footballappfinal.presenter

import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import me.ihwan.footballappfinal.model.PlayerDetailResponse
import me.ihwan.footballappfinal.model.PlayerResponse
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.network.TheSportsDBApi
import me.ihwan.footballappfinal.view.PlayerView
import org.jetbrains.anko.coroutines.experimental.bg

class PlayerPresenter (private val view: PlayerView,
                       private val apiRepository: ApiRepository,
                       private val gson: Gson) {

    fun getAllPlayers(team: String?) {
        async(UI) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportsDBApi.getSearchPlayers(team)),
                        PlayerResponse::class.java
                )
            }
            view.showPlayer(data.await().player)
        }
    }

    fun getDetailPlayer(playerId: String?) {
        async(UI) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportsDBApi.getDetailPlayers(playerId)),
                        PlayerDetailResponse::class.java
                )
            }
            view.showPlayer(data.await().players)
        }
    }
}