package me.ihwan.footballappfinal.presenter

import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import me.ihwan.footballappfinal.model.TeamResponse
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.network.TheSportsDBApi
import me.ihwan.footballappfinal.view.TeamView
import org.jetbrains.anko.coroutines.experimental.bg

class TeamPresenter(private val view: TeamView,
                    private val apiRepository: ApiRepository,
                    private val gson: Gson)  {


    fun getTeam(league: String){
        async(UI) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getTeam(league)), TeamResponse::class.java)
            }
            view.showTeam(data.await().teams)
        }
    }

    fun getSearchTeam(name: String){
        async(UI) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getSearchTeams(name)), TeamResponse::class.java)
            }
            view.showTeam(data.await().teams)
        }
    }
}
