package me.ihwan.footballappfinal.presenter

import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import me.ihwan.footballappfinal.model.EventResponse
import me.ihwan.footballappfinal.model.EventSearchResponse
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.network.TheSportsDBApi
import me.ihwan.footballappfinal.util.CoroutineContextProvider
import me.ihwan.footballappfinal.view.EventView
import org.jetbrains.anko.coroutines.experimental.bg

class EventPresenter(private val view: EventView,
                     private val apiRepository: ApiRepository,
                     private val gson: Gson,
                     private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getPastEvent(event: String?){
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getPast(event)), EventResponse::class.java)
            }
            view.showEvent(data.await().events)
        }
    }

    fun getNextEvent(event: String?){
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getNext(event)), EventResponse::class.java)
            }
            view.showEvent(data.await().events)
        }
    }

    fun getSearchEvent(query: String?){
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getSearchEvents(query)), EventSearchResponse::class.java)
            }
            view.showEvent(data.await().events)
        }
    }
}