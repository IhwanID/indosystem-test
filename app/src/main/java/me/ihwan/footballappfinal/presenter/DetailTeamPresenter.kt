package me.ihwan.footballappfinal.presenter

import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import me.ihwan.footballappfinal.model.TeamResponse
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.network.TheSportsDBApi
import me.ihwan.footballappfinal.util.CoroutineContextProvider
import me.ihwan.footballappfinal.view.DetailTeamView
import org.jetbrains.anko.coroutines.experimental.bg

class DetailTeamPresenter (private val view: DetailTeamView,
                           private val apiRepository: ApiRepository,
                           private val gson: Gson,
                           private val contextPool: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getTeamDetail(teamId: String) {

        async(contextPool.main){
            val data = bg{
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getDetailTeam(teamId)), TeamResponse::class.java)
            }

            view.showTeamDetail(data.await().teams)
        }
    }
}