package me.ihwan.footballappfinal.presenter

import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import me.ihwan.footballappfinal.model.EventResponse
import me.ihwan.footballappfinal.model.TeamResponse
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.network.TheSportsDBApi
import me.ihwan.footballappfinal.util.CoroutineContextProvider
import me.ihwan.footballappfinal.view.DetailEventView
import org.jetbrains.anko.coroutines.experimental.bg

class DetailEventPresenter (private val view: DetailEventView,
                            private val apiRepository: ApiRepository,
                            private val gson: Gson,
                            private val contexPool: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getBadge(home: String?, away: String?){
        async(contexPool.main) {
            val dataHome = bg {
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getDetailTeam(home)), TeamResponse::class.java)
            }
            val dataAway = bg {
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getDetailTeam(away)), TeamResponse::class.java)
            }

            view.showTeam(dataHome.await().teams, dataAway.await().teams)

        }
    }

    fun getDetail(team: String?){
        async(contexPool.main){
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportsDBApi.getDetailEvent(team)), EventResponse::class.java)
            }
            view.showEvent(data.await().events)
        }
    }
}