package me.ihwan.footballappfinal

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import me.ihwan.footballappfinal.model.Player
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.presenter.PlayerPresenter
import me.ihwan.footballappfinal.view.PlayerView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.themedAppBarLayout

class PlayerDetailActivity : AppCompatActivity() , PlayerView {

    private lateinit var presenter: PlayerPresenter
    private lateinit var imageView: ImageView
    private lateinit var playerHeight: TextView
    private lateinit var playerWeight: TextView
    private lateinit var playerPosition: TextView
    private lateinit var playerDescription: TextView
    private lateinit var toolbar: Toolbar
    private lateinit var playerId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        coordinatorLayout {

            lparams(width = matchParent, height = matchParent)
            fitsSystemWindows = true

            themedAppBarLayout(R.style.ThemeOverlay_AppCompat_Dark_ActionBar) {

                toolbar = themedToolbar {
                    R.style.ThemeOverlay_AppCompat_Light
                    backgroundColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
                }.lparams(width = matchParent, height = wrapContent) {
                    scrollFlags = 0
                }

            }.lparams(width = matchParent, height = wrapContent)

            linearLayout {

                lparams(width = matchParent, height = matchParent)
                orientation = LinearLayout.VERTICAL

                scrollView {
                    lparams(width = matchParent, height = wrapContent)

                    linearLayout {
                        padding = dip(16)
                        orientation = LinearLayout.VERTICAL
                        lparams(width = matchParent, height = matchParent)

                        imageView = imageView().lparams(width = matchParent, height = wrapContent) {
                            bottomMargin = dip(16)
                        }

                        playerHeight = textView {
                            textColor = Color.BLACK
                        }.lparams(width = wrapContent, height = wrapContent)

                        playerPosition = textView {
                            textColor = Color.BLACK
                        }.lparams(width = matchParent, height = wrapContent)

                        playerWeight = textView {
                            textColor = Color.BLACK
                        }.lparams(width = wrapContent, height = wrapContent)

                        playerDescription = textView {
                            textColor = Color.BLACK
                        }.lparams(width = matchParent, height = wrapContent) {
                            bottomMargin = dip(16)
                        }


                    }
                }

            }.lparams(width = matchParent, height = matchParent){
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }

        playerId = intent.getStringExtra("PlayerId") ?: "-"

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val request = ApiRepository()
        val gson = Gson()

        presenter = PlayerPresenter(this, request, gson)
        presenter.getDetailPlayer(playerId)

    }

    private fun initData(data: Player?){
        if (data != null){
            supportActionBar?.title = data.playerName
            Glide.with(ctx).load(data.playerThumb).into(imageView)
            playerPosition.text = "Position : " + data.playerPosition
            playerDescription.text ="Description : \n" + data.playerDescription
            playerHeight.text = "Height : " + data.playerHeight
            playerWeight.text = "Weight : " + data.playerWeight
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showPlayer(data: List<Player>?) {

        if (data != null && data.isNotEmpty()){
            initData(data[0]) }
    }
}
