package me.ihwan.footballappfinal.team

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import me.ihwan.footballappfinal.PlayerDetailActivity
import me.ihwan.footballappfinal.adapter.PlayerAdapter
import me.ihwan.footballappfinal.model.Player
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.presenter.PlayerPresenter
import me.ihwan.footballappfinal.view.PlayerView
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx

class PlayerFragment : Fragment() , AnkoComponent<Context>, PlayerView{

    companion object {
        const val TEAM_PLAYER = "TEAM_PLAYER"

        fun setInstance(teamPlayer : String) : PlayerFragment {
            val bindData = Bundle()
                bindData.putString(TEAM_PLAYER, teamPlayer)

            val fragment = PlayerFragment()
            fragment.arguments = bindData
            return fragment
        }
    }

    private var players: MutableList<Player> = mutableListOf()
    private lateinit var adapter: PlayerAdapter

    private lateinit var listPlayer: RecyclerView
    private lateinit var presenter: PlayerPresenter
    private lateinit var teamPlayer: String

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val bindData = arguments
        teamPlayer = bindData?.getString(TEAM_PLAYER) ?: "-"

        adapter = PlayerAdapter(players){
            ctx.startActivity<PlayerDetailActivity>("PlayerId" to "${it.playerId}")
        }
        listPlayer.adapter = adapter

        val request = ApiRepository()
        val gson = Gson()

        presenter = PlayerPresenter(this, request, gson)
        presenter.getAllPlayers(teamPlayer)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = matchParent)
                    listPlayer = recyclerView {
                        visibility = View.VISIBLE
                        layoutManager = LinearLayoutManager(ctx)

                    }.lparams(width = matchParent, height = matchParent)

        }
    }

    override fun showPlayer(data: List<Player>?) {
        players.clear()
        if (data != null && data.isNotEmpty()){
            players.addAll(data)
        }
        adapter.notifyDataSetChanged()
    }
}
