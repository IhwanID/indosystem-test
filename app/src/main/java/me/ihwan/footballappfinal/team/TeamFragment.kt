package me.ihwan.footballappfinal.team


import android.content.Context
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import com.google.gson.Gson
import me.ihwan.footballappfinal.R
import me.ihwan.footballappfinal.R.array.league
import me.ihwan.footballappfinal.TeamDetailActivity
import me.ihwan.footballappfinal.TeamSearchActivity
import me.ihwan.footballappfinal.adapter.TeamAdapter
import me.ihwan.footballappfinal.model.Team
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.presenter.TeamPresenter
import me.ihwan.footballappfinal.view.TeamView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.themedAppBarLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity

class TeamFragment : Fragment(), AnkoComponent<Context>, TeamView {
    private var teams: MutableList<Team> = mutableListOf()
    private lateinit var presenter: TeamPresenter
    private lateinit var adapter: TeamAdapter
    private lateinit var spinner: Spinner
    private lateinit var listTeam: RecyclerView
    private lateinit var leagueName: String
    private lateinit var toolbar: Toolbar
    private lateinit var strQuery: String

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setHasOptionsMenu(true)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        val spinnerItems = resources.getStringArray(league)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)

        spinner.adapter = spinnerAdapter

        adapter = TeamAdapter(teams) {
            ctx.startActivity<TeamDetailActivity>("team" to "${it.teamId}")
        }

        listTeam.adapter = adapter

        val request = ApiRepository()
        val gson = Gson()

        presenter = TeamPresenter(this, request, gson)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                leagueName = spinner.selectedItem.toString()
                presenter.getTeam(leagueName)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        strQuery = ""

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater?.inflate(R.menu.menu_search,menu)
        if (menu != null){
            val searchItem = menu.findItem(R.id.action_search)
            if (searchItem != null){
                val searchView = searchItem.actionView as SearchView

                searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        if (!TextUtils.isEmpty(query)){
                            startActivity<TeamSearchActivity>("query" to query)
                        }
                        return true
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        return false
                    }
                })

                searchView.setOnCloseListener {

                    true
                }
            }
        }
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){

        coordinatorLayout {

            lparams(width = matchParent, height = matchParent)

            fitsSystemWindows = true

            themedAppBarLayout(R.style.ThemeOverlay_AppCompat_Dark_ActionBar) {

                toolbar = themedToolbar {
                    R.style.ThemeOverlay_AppCompat_Light
                    backgroundColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
                }.lparams(width = matchParent, height = wrapContent) {
                    scrollFlags = 0
                }

            }.lparams(width = matchParent, height = wrapContent)

            linearLayout {
                lparams(width = matchParent, height = matchParent)
                orientation = LinearLayout.VERTICAL

                spinner = spinner ()

                    linearLayout{
                        lparams (width = matchParent, height = matchParent)

                        listTeam = recyclerView {
                            lparams (width = matchParent, height = matchParent)
                            layoutManager = LinearLayoutManager(ctx)
                        }
                    }

            }.lparams(width = matchParent, height = matchParent){
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }

    }

    override fun showTeam(data: List<Team>) {
        teams.clear()
        teams.addAll(data)
        adapter.notifyDataSetChanged()
    }


}
