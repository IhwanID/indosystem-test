package me.ihwan.footballappfinal.team

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.ctx

class OverviewFragment : Fragment(), AnkoComponent<Context> {

    private lateinit var textOverview: TextView
    private lateinit var teamOverview: String

    companion object {
        const val TEAM_OVERVIEW = "TEAM_OVERVIEW"

        fun setInstance(teamOverview : String) : OverviewFragment {
            val bindData = Bundle()
            bindData.putString(TEAM_OVERVIEW, teamOverview)

            val fragment = OverviewFragment()
            fragment.arguments = bindData
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val bindData = arguments
        teamOverview = bindData?.getString(TEAM_OVERVIEW) ?: "-"

        textOverview.text = teamOverview
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        
            scrollView {
                padding = dip(16)
                lparams(width = matchParent, height = matchParent)

                textOverview = textView {
                }.lparams(width = matchParent, height = wrapContent)

            }


    }
}
