package me.ihwan.footballappfinal.model

import com.google.gson.annotations.SerializedName

class Team (
        @SerializedName("idTeam") var teamId: String?,
        @SerializedName("strTeam") var teamName: String?,
        @SerializedName("strTeamBadge") var teamBadge: String?,
        @SerializedName("intFormedYear") var teamFormedYear: String?,
        @SerializedName("strStadium") var teamStadium: String?,
        @SerializedName("strDescriptionEN") var teamDescription: String?
)