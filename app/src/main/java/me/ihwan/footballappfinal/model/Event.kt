package me.ihwan.footballappfinal.model

import com.google.gson.annotations.SerializedName

data class Event(
    @SerializedName("idEvent") var eventId: String? = null,
    @SerializedName("idHomeTeam") var homeId: String? = null,
    @SerializedName("idAwayTeam") var awayId: String? = null,
    @SerializedName("strHomeTeam") var homeName: String? = null,
    @SerializedName("strAwayTeam") var awayName: String? = null,
    @SerializedName("intHomeScore") var homeScore: String? = null,
    @SerializedName("intAwayScore") var awayScore: String? = null,
    @SerializedName("dateEvent") var dateEvent: String? = null,
    @SerializedName("strHomeLineupGoalkeeper") var homeGoalKeeper: String? = null,
    @SerializedName("strAwayLineupGoalkeeper") var awayGoalKeeper: String? = null,
    @SerializedName("intHomeShots") var homeShots: String? = null,
    @SerializedName("intAwayShots") var awayShots: String? = null,
    @SerializedName("strHomeLineupDefense") var homeDefense: String? = null,
    @SerializedName("strAwayLineupDefense") var awayDefense: String? = null,
    @SerializedName("strHomeLineupMidfield") var homeMidfield: String? = null,
    @SerializedName("strAwayLineupMidfield") var awayMidfield: String? = null,
    @SerializedName("strHomeLineupSubstitutes") var homeSubstitutes: String? = null,
    @SerializedName("strAwayLineupSubstitutes") var awaySubstitutes: String? = null
)