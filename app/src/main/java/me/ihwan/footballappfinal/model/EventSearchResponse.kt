package me.ihwan.footballappfinal.model

import com.google.gson.annotations.SerializedName

class EventSearchResponse(@SerializedName("event")val events: List<Event>)