package me.ihwan.footballappfinal.model

import com.google.gson.annotations.SerializedName

class TeamResponse (
        @SerializedName("teams") val teams: List<Team>
)