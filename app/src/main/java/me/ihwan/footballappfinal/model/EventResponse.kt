package me.ihwan.footballappfinal.model

import com.google.gson.annotations.SerializedName

class EventResponse(
    @SerializedName("events") val events: List<Event>
)