package me.ihwan.footballappfinal.model

import com.google.gson.annotations.SerializedName

class Player(
        @SerializedName("idPlayer") var playerId: String?,
        @SerializedName("strPlayer") var playerName: String?,
        @SerializedName("strPosition") var playerPosition: String?,
        @SerializedName("strDescriptionEN") var playerDescription: String?,
        @SerializedName("strHeight") var playerHeight: String?,
        @SerializedName("strWeight") var playerWeight: String?,
        @SerializedName("strThumb") var playerThumb: String?,
        @SerializedName("strCutout") var playerCutout: String?
)