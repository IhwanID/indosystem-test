package me.ihwan.footballappfinal


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import me.ihwan.footballappfinal.event.EventFragment
import me.ihwan.footballappfinal.favorite.FavoriteFragment
import me.ihwan.footballappfinal.team.TeamFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.design.bottomNavigationView

class HomeActivity : AppCompatActivity() {

    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        relativeLayout {
            lparams(width = matchParent, height = matchParent)

            frameLayout {
                id = R.id.mainContainer
            }.lparams(width = matchParent, height = matchParent){
                above(R.id.bottomNavView)
            }

            view {
                background = ContextCompat.getDrawable(ctx, R.drawable.shadow)
            }.lparams(width = matchParent, height = dip(1)){
                above(R.id.bottomNavView)
            }

            bottomNavigationView = bottomNavigationView {
                id = R.id.bottomNavView

                inflateMenu(R.menu.navigation)
                itemBackgroundResource = android.R.color.white

            }.lparams(width = matchParent, height = wrapContent){
                alignParentBottom()
            }
        }

        supportFragmentManager
                .beginTransaction()
                .add(R.id.mainContainer, EventFragment())
                .commit()

        bottomNavigationView.setOnNavigationItemSelectedListener(
                BottomNavigationView.OnNavigationItemSelectedListener {
                    item ->  val fragment: Fragment?
                    when(item.itemId){
                        R.id.navigation_events -> {
                            fragment = EventFragment()
                            showFragment(fragment)
                            return@OnNavigationItemSelectedListener true
                        }

                        R.id.navigation_teams -> {
                            fragment = TeamFragment()
                            showFragment(fragment)
                            return@OnNavigationItemSelectedListener true
                        }

                        R.id.navigation_favorites -> {
                            fragment = FavoriteFragment()
                            showFragment(fragment)
                            return@OnNavigationItemSelectedListener true
                        }
                    }
                    false
                })

    }

    private fun showFragment(fragment: Fragment?){
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.mainContainer, fragment)
                .commit()
    }

}
