package me.ihwan.footballappfinal

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import me.ihwan.footballappfinal.adapter.ViewPagerAdapter
import me.ihwan.footballappfinal.database.database
import me.ihwan.footballappfinal.model.Team
import me.ihwan.footballappfinal.model.TeamFavorite
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.presenter.DetailTeamPresenter
import me.ihwan.footballappfinal.team.OverviewFragment
import me.ihwan.footballappfinal.team.PlayerFragment
import me.ihwan.footballappfinal.view.DetailTeamView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.collapsingToolbarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.design.themedAppBarLayout
import org.jetbrains.anko.support.v4.viewPager

class TeamDetailActivity : AppCompatActivity(), DetailTeamView {



    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager
    private lateinit var toolbar: Toolbar
    private lateinit var presenter: DetailTeamPresenter
    private lateinit var teams: Team
    private lateinit var teamBadge: ImageView
    private lateinit var teamName: TextView
    private lateinit var teamFormedYear: TextView
    private lateinit var teamStadium: TextView
    private lateinit var nameTeam: String
    private lateinit var overviewTeam: String
    private lateinit var id: String

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coordinatorLayout = coordinatorLayout {
            lparams(width = matchParent, height = matchParent)
            fitsSystemWindows = true

            themedAppBarLayout(R.style.ThemeOverlay_AppCompat_Dark_ActionBar) {

                collapsingToolbarLayout {
                    toolbar = themedToolbar {
                        R.style.ThemeOverlay_AppCompat_Light
                        backgroundColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
                    }.lparams(width = matchParent, height = dip(300)) {
                        collapseMode = CollapsingToolbarLayout.LayoutParams.COLLAPSE_MODE_PARALLAX
                    }

                    linearLayout {
                        orientation = LinearLayout.VERTICAL

                        teamBadge = imageView().lparams(width = dip(100), height = dip(100)){
                            bottomMargin = dip(10)
                            gravity = Gravity.CENTER_HORIZONTAL
                        }

                        teamName = textView{
                            textSize = 10f
                            gravity = Gravity.CENTER
                        }.lparams(width = matchParent, height = wrapContent)

                        teamFormedYear = textView{
                            textSize = 8f
                            gravity = Gravity.CENTER
                        }.lparams(width = matchParent, height = wrapContent)

                        teamStadium = textView{
                            textSize = 8f
                            gravity = Gravity.CENTER
                        }.lparams(width = matchParent, height = wrapContent)

                    }.lparams(width = matchParent, height = wrapContent){
                        gravity = Gravity.CENTER
                    }

                    tabLayout = tabLayout{
                        lparams(width = matchParent, height = wrapContent)
                        tabMode = TabLayout.MODE_FIXED
                    }.lparams(width = matchParent, height = wrapContent){
                        gravity = Gravity.BOTTOM
                    }

                }.lparams(width = matchParent, height = matchParent) {
                    scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS_COLLAPSED
                }

            }.lparams(width = matchParent, height = wrapContent)

            linearLayout {

                viewPager = viewPager{
                    id = R.id.teamViewPager
                }

            }.lparams(width = matchParent, height = matchParent){
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }


        val intent = intent
        id = intent.getStringExtra("team")

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        favoriteState()

        presenter = DetailTeamPresenter(this, ApiRepository(), Gson())
        presenter.getTeamDetail(id)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_favorite, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.button_favorite -> {
                if (isFavorite) removeFavorite() else addFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addFavorite(){
        try {
            database.use {
                insert(TeamFavorite.TABLE_TEAM_FAVORITE,
                        TeamFavorite.TEAM_ID to teams.teamId,
                        TeamFavorite.TEAM_NAME to teams.teamName,
                        TeamFavorite.TEAM_BADGE to teams.teamBadge,
                        TeamFavorite.TEAM_FORMED_YEAR to teams.teamFormedYear,
                        TeamFavorite.TEAM_STADIUM to teams.teamStadium,
                        TeamFavorite.TEAM_DESC to teams.teamDescription)
            }
            toast("Saved!")
        } catch (e: SQLiteConstraintException){
            toast("error!")
        }
    }

    private fun removeFavorite(){
        try {
            database.use {
                delete(TeamFavorite.TABLE_TEAM_FAVORITE, "(TEAM_ID = {id})", "id" to id)
            }
            toast("Deleted!")
        } catch (e: SQLiteConstraintException){
            toast("error deleted!")
        }
    }

    private fun setFavorite() {
        if (isFavorite) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorited)
        }
        else
        {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite)
        }
    }

    private fun favoriteState(){
        database.use {
            val result = select(TeamFavorite.TABLE_TEAM_FAVORITE).whereArgs("(TEAM_ID = {id})", "id" to id)
            val Teamfavorite = result.parseList(classParser<TeamFavorite>())
            if (!Teamfavorite.isEmpty()) isFavorite = true
        }
    }


    override fun showTeamDetail(data: List<Team>) {
        teams = Team(data[0].teamId,
                data[0].teamName,
                data[0].teamBadge,
                data[0].teamFormedYear,
                data[0].teamStadium,
                data[0].teamDescription)

        Glide.with(this).load(data[0].teamBadge).into(teamBadge)
        teamName.text = data[0].teamName

        teamFormedYear.text = data[0].teamFormedYear
        teamStadium.text = data[0].teamStadium

        overviewTeam = data[0].teamDescription ?: "-"
        nameTeam = data[0].teamName ?: "-"

        setupViewpager(viewPager)
        tabLayout.setupWithViewPager(viewPager)

    }

    private fun setupViewpager(viewPager: ViewPager){
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addFragment(OverviewFragment.setInstance(overviewTeam), "OVERVIEW")
        viewPagerAdapter.addFragment(PlayerFragment.setInstance(nameTeam), "PLAYER")
        viewPager.adapter = viewPagerAdapter
    }
}
