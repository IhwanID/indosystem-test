package me.ihwan.footballappfinal.event

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.google.gson.Gson
import me.ihwan.footballappfinal.EventDetailActivity
import me.ihwan.footballappfinal.R
import me.ihwan.footballappfinal.adapter.EventAdapter
import me.ihwan.footballappfinal.model.Event
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.presenter.EventPresenter
import me.ihwan.footballappfinal.view.EventView
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.startActivity


class NextFragment : Fragment(), AnkoComponent<Context>, EventView {
    private var events: MutableList<Event> = mutableListOf()
    private lateinit var adapter: EventAdapter
    private lateinit var listNext: RecyclerView
    private lateinit var spinner: Spinner
    private lateinit var presenter: EventPresenter
    private lateinit var leagueId: String
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        spinner = find(R.id.spinnerEvent)
        listNext = find(R.id.eventRecyclerView)

        val spinnerItems = resources.getStringArray(R.array.league)
        val leagueIds = resources.getIntArray(R.array.leagueId)

        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter

        adapter = EventAdapter(events) {
            startActivity<EventDetailActivity>("eventId" to it.eventId)
        }

        listNext.adapter = adapter

        val request = ApiRepository()
        val gson = Gson()

        presenter = EventPresenter(this, request, gson)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                leagueId = leagueIds[position].toString()
                presenter.getNextEvent(leagueId)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }


    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        customEventListLayout{}
    }

    private inline fun ViewManager.customEventListLayout(theme: Int = 0, init: EventListLayout.() -> Unit): EventListLayout {
        return ankoView({ EventListLayout(it) }, theme = theme, init = init)
    }

    override fun showEvent(data: List<Event>) {
        events.clear()
        events.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

}
