package me.ihwan.footballappfinal.event


import android.content.Context
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.*
import me.ihwan.footballappfinal.EventSearchActivity
import me.ihwan.footballappfinal.R
import me.ihwan.footballappfinal.adapter.ViewPagerAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.design.themedAppBarLayout
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.viewPager

class EventFragment : Fragment(), AnkoComponent<Context> {

    private lateinit var toolbar: Toolbar
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager

    override fun createView(ui: AnkoContext<Context>): View = with(ui){

        coordinatorLayout {
            lparams(width = matchParent, height = matchParent)
            fitsSystemWindows = true

            themedAppBarLayout(R.style.ThemeOverlay_AppCompat_Dark_ActionBar) {

                toolbar = themedToolbar {
                    R.style.ThemeOverlay_AppCompat_Light
                    backgroundColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
                }.lparams(width = matchParent, height = wrapContent) {
                    scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
                }

                tabLayout = tabLayout{
                    id = R.id.tabLayoutEvent
                    lparams(width = matchParent, height = wrapContent)
                    backgroundColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
                    tabMode = TabLayout.MODE_FIXED
                }.lparams(width = matchParent, height = wrapContent){
                    scrollFlags = 0
                }
            }.lparams(width = matchParent, height = wrapContent)

            viewPager = viewPager {
                id = R.id.viewPagerEvent
            }.lparams(width = matchParent, height = matchParent){
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        setupViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun setupViewPager(viewPager: ViewPager){
        val viewPagerAdapter = ViewPagerAdapter(childFragmentManager)
        viewPagerAdapter.addFragment(PastFragment(), "PAST")
        viewPagerAdapter.addFragment(NextFragment(), "NEXT")
        viewPager.adapter = viewPagerAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater?.inflate(R.menu.menu_search,menu)

        if (menu != null){
            val searchItem = menu.findItem(R.id.action_search)
            if (searchItem != null){
                val searchView = searchItem.actionView as SearchView
                searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        if (!TextUtils.isEmpty(query)){
                            startActivity<EventSearchActivity>("query" to query)}
                        return true }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        return false }
                })

                searchView.setOnCloseListener {
                    true
                }
            }
        }
    }

}
