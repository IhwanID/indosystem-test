package me.ihwan.footballappfinal.event

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import me.ihwan.footballappfinal.R
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class EventListLayout(ctx: Context): LinearLayout(ctx) {

    init {
        linearLayout {
            lparams(width = matchParent, height = matchParent)
            orientation = LinearLayout.VERTICAL

            spinner {
                id = R.id.spinnerEvent
            }.lparams(width = matchParent, height = wrapContent){
                bottomMargin = dip(10)
            }

            linearLayout{
                lparams(width = matchParent, height = matchParent)

                recyclerView {
                    id = R.id.eventRecyclerView
                    layoutManager = LinearLayoutManager(ctx)
                }.lparams(width = matchParent, height = matchParent)
            }
        }
    }
}