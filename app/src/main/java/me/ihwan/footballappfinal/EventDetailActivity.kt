package me.ihwan.footballappfinal

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import me.ihwan.footballappfinal.R.drawable.ic_favorite
import me.ihwan.footballappfinal.R.drawable.ic_favorited
import me.ihwan.footballappfinal.R.id.*
import me.ihwan.footballappfinal.R.menu.menu_favorite
import me.ihwan.footballappfinal.database.database
import me.ihwan.footballappfinal.model.Event
import me.ihwan.footballappfinal.model.EventFavorite
import me.ihwan.footballappfinal.model.Team
import me.ihwan.footballappfinal.network.ApiRepository
import me.ihwan.footballappfinal.presenter.DetailEventPresenter
import me.ihwan.footballappfinal.util.toSimpleDate
import me.ihwan.footballappfinal.view.DetailEventView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.themedAppBarLayout
import java.text.SimpleDateFormat
import java.util.*

class EventDetailActivity : AppCompatActivity(), DetailEventView {

    private lateinit var toolbar: Toolbar
    private lateinit var presenter: DetailEventPresenter
    private lateinit var events: Event
    private lateinit var eventId: String
    private lateinit var dateEvent: TextView
    private lateinit var homeTeam: TextView
    private lateinit var awayTeam: TextView
    private lateinit var homeScore: TextView
    private lateinit var awayScore: TextView
    private lateinit var homeShot: TextView
    private lateinit var awayShot: TextView
    private lateinit var homeGK: TextView
    private lateinit var awayGK: TextView
    private lateinit var homeDefense: TextView
    private lateinit var awayDefense: TextView
    private lateinit var homeMidfield: TextView
    private lateinit var awayMidfield: TextView
    private lateinit var homeSubstitute: TextView
    private lateinit var awaySubstitute: TextView
    private lateinit var homeTeamBadge: ImageView
    private lateinit var awayTeamBadge: ImageView

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        coordinatorLayout {

            lparams(width = matchParent, height = matchParent)
            fitsSystemWindows = true

            themedAppBarLayout(R.style.ThemeOverlay_AppCompat_Dark_ActionBar) {

                toolbar = themedToolbar {
                    R.style.ThemeOverlay_AppCompat_Light
                }.lparams(width = matchParent, height = wrapContent)

            }.lparams(width = matchParent, height = wrapContent)

            linearLayout {
                lparams(width = matchParent, height = matchParent)
                orientation = LinearLayout.VERTICAL

                scrollView {
                    lparams(width = matchParent, height = wrapContent)

                    linearLayout {
                        lparams(width = matchParent, height = matchParent)
                        orientation = LinearLayout.VERTICAL
                        horizontalPadding = dip(10)
                        verticalPadding = dip(5)

                       dateEvent = textView {
                           gravity = Gravity.CENTER
                        }.lparams(width = matchParent, height = wrapContent)


                        linearLayout {
                            orientation = LinearLayout.HORIZONTAL
                            weightSum = 1f

                            relativeLayout {
                                padding = dip(4)

                                linearLayout {
                                    orientation = LinearLayout.VERTICAL

                                    homeTeamBadge = imageView().lparams(width = dip(60), height = dip(60)){
                                        gravity = Gravity.CENTER
                                    }

                                }.lparams(width = wrapContent, height = matchParent){
                                    alignParentLeft()
                                    leftOf(R.id.homeScore)
                                    gravity = Gravity.CENTER
                                }

                                homeScore = textView {
                                    id = R.id.homeScore
                                    textSize = 40f
                                    gravity = Gravity.CENTER
                                }.lparams(width = wrapContent, height = matchParent){
                                    alignParentRight()
                                    gravity = Gravity.CENTER_VERTICAL
                                }

                            }.lparams(width = dip(0), height = wrapContent){
                                weight = 0.45f
                                gravity = RelativeLayout.CENTER_VERTICAL
                            }

                            textView {
                                text = ctx.getString(R.string.versus)
                                textSize = 11f

                            }.lparams(width = dip(0), height = matchParent){
                                weight = 0.1f
                                gravity = Gravity.CENTER_HORIZONTAL
                            }.gravity = Gravity.CENTER

                            relativeLayout {
                                padding = dip(4)

                                linearLayout {
                                    orientation = LinearLayout.VERTICAL

                                    awayTeamBadge = imageView().lparams(width = dip(60), height = dip(60)){
                                        gravity = Gravity.CENTER
                                    }

                                }.lparams(width = wrapContent, height = matchParent){
                                    alignParentRight()
                                    rightOf(R.id.awayScore)
                                    gravity = Gravity.CENTER
                                }


                                awayScore = textView {
                                    id = R.id.awayScore
                                    textSize = 40f
                                    gravity = Gravity.CENTER

                                }.lparams(width = wrapContent, height = matchParent){
                                    alignParentLeft()
                                    gravity = Gravity.CENTER_HORIZONTAL
                                }

                            }.lparams(width = dip(0), height = wrapContent){
                                weight = 0.45f
                                gravity = RelativeLayout.CENTER_VERTICAL
                            }
                        }.lparams(width = matchParent, height = wrapContent){
                        }

                        linearLayout {
                            padding = dip(6)
                            orientation = LinearLayout.VERTICAL

                            //TeamName
                            linearLayout {
                                weightSum = 1f

                                homeTeam = textView {
                                    id = R.id.homeTeam
                                    gravity = Gravity.START
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.5f
                                }

                                awayTeam = textView {
                                    id = R.id.awayTeam
                                    gravity = Gravity.END
                                }.lparams(width = dip(0), height = matchParent) {
                                    weight = 0.5f
                                }

                            }.lparams(width = matchParent, height = wrapContent){
                                bottomMargin = dip(20)
                            }

                            //Shot
                            linearLayout {
                                weightSum = 1f

                                homeShot = textView {
                                    gravity = Gravity.START
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                                textView {
                                    text = ctx.getString(R.string.shot)
                                    textSize = 11f
                                    gravity = Gravity.CENTER_HORIZONTAL
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.2f
                                }

                                awayShot = textView {
                                    textSize = 12f
                                    gravity = Gravity.END
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                            }.lparams(width = matchParent, height = wrapContent){
                                bottomMargin = dip(20)
                            }

                            //Goal Keeper
                            linearLayout {
                                weightSum = 1f

                                homeGK = textView {
                                    gravity = Gravity.START
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                                textView {
                                    text = ctx.getString(R.string.gk)
                                    textSize = 11f
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.2f
                                }.gravity = Gravity.CENTER_HORIZONTAL

                                awayGK = textView {
                                    gravity = Gravity.END
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                            }.lparams(width = matchParent, height = wrapContent){
                            }

                            //Defense
                            linearLayout {
                                weightSum = 1f

                                homeDefense = textView {
                                    gravity = Gravity.START
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                                textView {
                                    text = ctx.getString(R.string.defense)
                                    textSize = 11f
                                    gravity = Gravity.CENTER_HORIZONTAL
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.2f
                                }

                                awayDefense = textView {
                                    gravity = Gravity.END
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                            }.lparams(width = matchParent, height = wrapContent){
                                bottomMargin = dip(10)
                            }

                            //Midfield
                            linearLayout {
                                weightSum = 1f

                                homeMidfield = textView {
                                    gravity = Gravity.START
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                                textView {
                                    text = ctx.getString(R.string.Midfield)
                                    textSize = 11f
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.2f
                                }.gravity = Gravity.CENTER_HORIZONTAL

                               awayMidfield = textView {
                                   gravity = Gravity.END
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                            }.lparams(width = matchParent, height = wrapContent){
                                bottomMargin = dip(10)
                            }

                            //Subtitutes
                            linearLayout {
                                weightSum = 1f

                                homeSubstitute = textView {
                                    gravity = Gravity.START
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                                textView {
                                    text = ctx.getString(R.string.substitute)
                                    textSize = 11f
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.2f
                                }.gravity = Gravity.CENTER_HORIZONTAL

                                awaySubstitute = textView {
                                    gravity = Gravity.END
                                }.lparams(width = dip(0), height = wrapContent){
                                    weight = 0.4f
                                }

                            }.lparams(width = matchParent, height = wrapContent){
                                bottomMargin = dip(10)
                            }



                        }.lparams(width = matchParent, height = wrapContent)
                    }
                }

            }.lparams(width = matchParent, height = matchParent){
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Detail Match"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        eventId = intent.getStringExtra("eventId")
        favoriteState()

        presenter = DetailEventPresenter(this, ApiRepository(), Gson())
        presenter.getDetail(eventId)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(menu_favorite, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            button_favorite -> {
                if (isFavorite) removeFavorite() else addFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addFavorite(){
        try {
            database.use {
                insert(EventFavorite.TABLE_EVENT_FAVORITE,
                        EventFavorite.EVENT_ID to events.eventId,
                        EventFavorite.EVENT_DATE to events.dateEvent,
                        EventFavorite.HOME_TEAM to events.homeName,
                        EventFavorite.AWAY_TEAM to events.awayName,
                        EventFavorite.HOME_SCORE to events.homeScore,
                        EventFavorite.AWAY_SCORE to events.awayScore)
            }
            toast("Saved!")
        } catch (e: SQLiteConstraintException){
            toast("error!")
        }
    }

    private fun removeFavorite(){
        try {
            database.use {
                delete(EventFavorite.TABLE_EVENT_FAVORITE, "(EVENT_ID = {id})", "id" to eventId)
            }
            toast("Deleted!")
        } catch (e: SQLiteConstraintException){
            toast("error deleted!")
        }
    }

    private fun setFavorite() {
        if (isFavorite) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_favorited)
        }
        else
        {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_favorite)
        }
    }

    private fun favoriteState(){
        database.use {
            val result = select(EventFavorite.TABLE_EVENT_FAVORITE).whereArgs("(EVENT_ID = {id})", "id" to eventId)
            val eventFavorite = result.parseList(classParser<EventFavorite>())
            if (!eventFavorite.isEmpty()) isFavorite = true
        }
    }


    override fun showTeam(dataHome: List<Team>, dataAway: List<Team>) {
        Glide.with(this).load(dataHome[0].teamBadge).into(homeTeamBadge)
        Glide.with(this).load(dataAway[0].teamBadge).into(awayTeamBadge)
    }

    override fun showEvent(data: List<Event>) {
        events = Event(data[0].eventId,
                data[0].homeId,
                data[0].awayId,
                data[0].homeName,
                data[0].awayName,
                data[0].homeScore,
                data[0].awayScore,
                data[0].dateEvent,
                data[0].homeGoalKeeper,
                data[0].awayGoalKeeper,
                data[0].homeShots,
                data[0].awayShots,
                data[0].homeDefense,
                data[0].awayDefense,
                data[0].homeMidfield,
                data[0].awayMidfield,
                data[0].homeSubstitutes,
                data[0].awaySubstitutes)

        var formatedDate = "-"
        if (data[0].dateEvent != null){
            val dates: Date = SimpleDateFormat("yyyy-MM-dd").parse(data[0].dateEvent)
            formatedDate = toSimpleDate(dates) ?: "-" }



        dateEvent.text = formatedDate
        homeTeam.text = data[0].homeName
        awayTeam.text = data[0].awayName
        homeScore.text = data[0].homeScore
        awayScore.text = data[0].awayScore
        homeShot.text = data[0].homeShots
        awayShot.text = data[0].awayShots
        homeGK.text = data[0].homeGoalKeeper
        awayGK.text = data[0].awayGoalKeeper
        homeDefense.text = data[0].homeDefense
        awayDefense.text = data[0].awayDefense
        homeMidfield.text = data[0].homeMidfield
        awayMidfield.text = data[0].awayMidfield
        homeSubstitute.text = data[0].homeSubstitutes
        awaySubstitute.text = data[0].awaySubstitutes

        presenter.getBadge(data[0].homeId, data[0].awayId)
    }
}
