package me.ihwan.footballappfinal.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import me.ihwan.footballappfinal.model.EventFavorite
import me.ihwan.footballappfinal.model.TeamFavorite
import org.jetbrains.anko.db.*

class DatabaseHelper(ctx: Context): ManagedSQLiteOpenHelper(ctx,"FavoriteTeam.db",null,1){

    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(ctx: Context):DatabaseHelper{
            if (instance==null){
                instance = DatabaseHelper(ctx.applicationContext)
            }
            return instance as DatabaseHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(TeamFavorite.TABLE_TEAM_FAVORITE, true,
                TeamFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                TeamFavorite.TEAM_ID to TEXT + UNIQUE,
                TeamFavorite.TEAM_NAME to TEXT,
                TeamFavorite.TEAM_BADGE to TEXT,
                TeamFavorite.TEAM_FORMED_YEAR to TEXT,
                TeamFavorite.TEAM_STADIUM to TEXT,
                TeamFavorite.TEAM_DESC to TEXT)

        db?.createTable(EventFavorite.TABLE_EVENT_FAVORITE, true,
                EventFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                EventFavorite.EVENT_ID to TEXT + UNIQUE,
                EventFavorite.EVENT_DATE to TEXT,
                EventFavorite.HOME_TEAM to TEXT,
                EventFavorite.AWAY_TEAM to TEXT,
                EventFavorite.HOME_SCORE to TEXT,
                EventFavorite.AWAY_SCORE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(EventFavorite.TABLE_EVENT_FAVORITE,true)
        db.dropTable(TeamFavorite.TABLE_TEAM_FAVORITE,true)
    }

}

val Context.database: DatabaseHelper
    get() = DatabaseHelper.getInstance(applicationContext)