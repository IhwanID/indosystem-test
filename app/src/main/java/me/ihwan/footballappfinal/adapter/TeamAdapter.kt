package me.ihwan.footballappfinal.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import me.ihwan.footballappfinal.R
import me.ihwan.footballappfinal.R.id.teamBadge
import me.ihwan.footballappfinal.R.id.teamName
import me.ihwan.footballappfinal.model.Team
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick


class TeamAdapter (private val teams: List<Team>, private val listener: (Team) -> Unit)
    : RecyclerView.Adapter<TeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder
            = TeamViewHolder(TeamUI().createView(AnkoContext.create(parent.context, parent)))

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int)
            = holder.bindItem(teams[position], listener)
}

class TeamUI: AnkoComponent<ViewGroup>{
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui){

            cardView {
                id = R.id.TeamCardView

                lparams(width = matchParent, height = wrapContent) {
                    topMargin = dip(8)
                    rightMargin = dip(5)
                    leftMargin = dip(5)
                }
                linearLayout{
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.HORIZONTAL
                    padding = dip(16)

                    imageView {
                        id = teamBadge
                    }.lparams{
                        height = dip(50)
                        width = dip(50)
                    }

                    textView {
                        id = teamName
                        textSize = 16f
                    }.lparams{
                        margin = dip(15)
                    }
                }
            }
        }
    }
}

class TeamViewHolder(view: View): RecyclerView.ViewHolder(view){

    private val teamBadge: ImageView = view.find(R.id.teamBadge)
    private val teamName: TextView = view.find(R.id.teamName)

    fun bindItem(teams: Team, listener: (Team) -> Unit){
        Glide.with(itemView.context).load(teams.teamBadge).into(teamBadge)
        teamName.text = teams.teamName
        itemView.onClick { listener(teams) }
    }
}