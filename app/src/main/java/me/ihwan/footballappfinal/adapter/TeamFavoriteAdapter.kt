package me.ihwan.footballappfinal.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import me.ihwan.footballappfinal.R
import me.ihwan.footballappfinal.R.id.teamBadge
import me.ihwan.footballappfinal.R.id.teamName
import me.ihwan.footballappfinal.model.TeamFavorite
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick


class TeamFavoriteAdapter(private val teamFavorites: List<TeamFavorite>, private val listener: (TeamFavorite) -> Unit)
    : RecyclerView.Adapter<TeamFavoriteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamFavoriteViewHolder
            = TeamFavoriteViewHolder(TeamFavoriteUI().createView(AnkoContext.create(parent.context, parent)))

    override fun getItemCount(): Int = teamFavorites.size

    override fun onBindViewHolder(holder: TeamFavoriteViewHolder, position: Int)
            = holder.bindItem(teamFavorites[position], listener)
}

class TeamFavoriteUI: AnkoComponent<ViewGroup>{
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui){

            cardView {
                id = R.id.TeamCardView

                lparams(width = matchParent, height = wrapContent) {
                    topMargin = dip(8)
                    rightMargin = dip(5)
                    leftMargin = dip(5)
                }
                linearLayout{
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.HORIZONTAL
                    padding = dip(16)

                    imageView {
                        id = teamBadge
                    }.lparams{
                        height = dip(50)
                        width = dip(50)
                    }

                    textView {
                        id = teamName
                        textSize = 16f
                    }.lparams{
                        margin = dip(15)
                    }
                }
            }
        }
    }
}

class TeamFavoriteViewHolder(view: View): RecyclerView.ViewHolder(view){

    private val teamBadge: ImageView = view.find(R.id.teamBadge)
    private val teamName: TextView = view.find(R.id.teamName)

    fun bindItem(teamfavorites: TeamFavorite, listener: (TeamFavorite) -> Unit){
        Glide.with(itemView.context).load(teamfavorites.teamBadge).into(teamBadge)
        teamName.text = teamfavorites.teamName
        itemView.onClick { listener(teamfavorites) }
    }
}