package me.ihwan.footballappfinal.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import me.ihwan.footballappfinal.R
import me.ihwan.footballappfinal.model.Player
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick

class PlayerAdapter (private val players: List<Player>,  private val listener: (Player) -> Unit)
    : RecyclerView.Adapter<PlayerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder
        = PlayerViewHolder(PlayerUI().createView(AnkoContext.create(parent.context, parent)))

    override fun getItemCount(): Int = players.size

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) = holder.bindItem(players[position], listener)
}

class PlayerUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {

            cardView {
                id = R.id.playerCardView

                lparams(width = matchParent, height = wrapContent) {
                    topMargin = dip(8)
                    rightMargin = dip(5)
                    leftMargin = dip(5)
                }

                linearLayout {
                    padding = dip(8)
                    orientation = LinearLayout.HORIZONTAL
                    weightSum = 1f
                    lparams(width = matchParent, height = wrapContent)

                    imageView {
                        id = R.id.playerImage
                    }.lparams {
                        height = dip(50)
                        width = dip(0)
                        rightMargin = dip(10)
                        weight = 0.2f
                    }

                    textView {
                        id = R.id.playerName
                        textSize = 14f
                    }.lparams(width = dip(0), height = wrapContent) {
                        rightMargin = dip(20)
                        weight = 0.6f
                        gravity = Gravity.CENTER
                    }

                }
            }
        }
    }
}

class PlayerViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val playerLayout: CardView = view.find(R.id.playerCardView)
    private val playerImage: ImageView = view.find(R.id.playerImage)
    private val playerName: TextView = view.find(R.id.playerName)

    fun bindItem(player : Player, listener: (Player) -> Unit){

        if (player.playerCutout != null && !TextUtils.isEmpty(player.playerCutout)){
            Glide.with(itemView.context).load(player.playerCutout).into(playerImage)
        }

        playerName.text = player.playerName ?: "-"

        playerLayout.onClick { listener(player) }
    }
}
