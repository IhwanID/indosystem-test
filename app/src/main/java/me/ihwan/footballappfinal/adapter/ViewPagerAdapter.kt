package me.ihwan.footballappfinal.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class ViewPagerAdapter(fragmentManager: FragmentManager)
    : FragmentStatePagerAdapter(fragmentManager) {

    private val fragmentName: MutableList<Fragment> = mutableListOf()
    private val titleFragment: MutableList<String> = mutableListOf()

    override fun getItem(position: Int): Fragment = fragmentName[position]

    override fun getCount(): Int = fragmentName.size

    fun addFragment(fragment: Fragment, title: String){
        fragmentName.add(fragment)
        titleFragment.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence? = titleFragment[position]

}