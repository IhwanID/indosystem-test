package me.ihwan.footballappfinal.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import me.ihwan.footballappfinal.R
import me.ihwan.footballappfinal.model.Event
import me.ihwan.footballappfinal.util.toSimpleDate
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.*

class EventAdapter(private val events: List<Event>, private val listener: (Event) -> Unit)
    : RecyclerView.Adapter<EventViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):EventViewHolder =
            EventViewHolder(EventUI().createView(AnkoContext.create(parent.context, parent)))

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) = holder.bindEvent(events[position], listener)
}

class EventUI: AnkoComponent<ViewGroup>{
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {

            cardView {
                id = R.id.eventCardView

                lparams(width= matchParent, height = wrapContent){
                    topMargin = dip(8)
                    rightMargin = dip(5)
                    leftMargin = dip(5)
                }

                linearLayout {
                    orientation = LinearLayout.VERTICAL
                    padding = dip(16)

                    textView {
                        id = R.id.dateEvent
                        gravity = Gravity.CENTER
                        textSize = 14f
                        bottomPadding = dip(16)
                    }.lparams(width = matchParent, height = wrapContent)

                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)
                        orientation = LinearLayout.HORIZONTAL

                        textView {
                            id = R.id.homeTeam
                            gravity = Gravity.CENTER
                        }.lparams{
                            width = matchParent
                            weight = 1f
                        }

                        textView {
                            id = R.id.matchScore
                            gravity = Gravity.CENTER
                        }.lparams {
                            width = matchParent
                            weight = 1f
                        }

                        textView {
                            id = R.id.awayTeam
                            gravity = Gravity.CENTER
                        }.lparams {
                            width = matchParent
                            weight = 1f
                        }

                    }
                }
            }
        }
    }
}

class EventViewHolder(view: View): RecyclerView.ViewHolder(view){

    private val cardViewEvent: CardView = view.find(R.id.eventCardView)
    private val date: TextView = view.find(R.id.dateEvent)
    private val homeTeam: TextView = view.find(R.id.homeTeam)
    private val awayTeam: TextView = view.find(R.id.awayTeam)
    private val scoreEvent: TextView = view.find(R.id.matchScore)

    fun bindEvent(events: Event, listener: (Event) -> Unit){

        var formatedDate = "-"
        if (events.dateEvent != null){
            val dates: Date = SimpleDateFormat("yyyy-MM-dd").parse(events.dateEvent)
            formatedDate = toSimpleDate(dates) ?: "-" }

        date.text = formatedDate
        homeTeam.text = events.homeName
        awayTeam.text = events.awayName

        if (events.homeScore != null && events.awayScore != null){
            scoreEvent.text = events.homeScore + " VS " + events.awayScore }
        else{scoreEvent.text = "- VS -"}

        cardViewEvent.onClick { listener(events) }
    }

}