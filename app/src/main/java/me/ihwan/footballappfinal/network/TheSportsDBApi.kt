package me.ihwan.footballappfinal.network

import me.ihwan.footballappfinal.BuildConfig.API_KEY
import me.ihwan.footballappfinal.BuildConfig.BASE_URL

object TheSportsDBApi {

    fun getPast(pastLeague: String?): String{
        return BASE_URL +"api/v1/json/$API_KEY/eventspastleague.php?id=$pastLeague"
    }

    fun getNext(nextLeague: String?): String{
        return BASE_URL +"api/v1/json/$API_KEY/eventsnextleague.php?id=$nextLeague"
    }

    fun getDetailEvent(detailEvent: String?): String{
        return BASE_URL +"api/v1/json/$API_KEY/lookupevent.php?id=$detailEvent"
    }

    fun     getDetailTeam(detailTeam : String?): String{
        return BASE_URL + "api/v1/json/$API_KEY/lookupteam.php?id=$detailTeam"
    }

    fun getSearchEvents(query: String?): String{
        return BASE_URL + "api/v1/json/$API_KEY/searchevents.php?e=$query"
    }

    fun getSearchTeams(team: String?): String{
        return BASE_URL + "api/v1/json/$API_KEY/searchteams.php?t=$team"
    }

    fun getSearchPlayers(team: String?): String{
        return BASE_URL + "api/v1/json/$API_KEY/searchplayers.php?t=$team"
    }
    fun getDetailPlayers(playerId: String?): String{
        return BASE_URL + "api/v1/json/$API_KEY/lookupplayer.php?id=$playerId"
    }

    fun getTeam(team: String?): String{
        return BASE_URL + "api/v1/json/$API_KEY/search_all_teams.php?l=$team"
    }

}