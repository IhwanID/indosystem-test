package me.ihwan.footballappfinal.favorite


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.ihwan.footballappfinal.TeamDetailActivity
import me.ihwan.footballappfinal.adapter.TeamFavoriteAdapter
import me.ihwan.footballappfinal.database.database
import me.ihwan.footballappfinal.model.TeamFavorite
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity


class TeamFavoriteFragment : Fragment(), AnkoComponent<Context> {

    private var teamFavorites: MutableList<TeamFavorite> = mutableListOf()
    private lateinit var adapter: TeamFavoriteAdapter
    private lateinit var listTeamFavorite: RecyclerView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = TeamFavoriteAdapter(teamFavorites){
            startActivity<TeamDetailActivity>("team" to it.teamId)
        }
        listTeamFavorite.adapter = adapter

        showFavorite()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams(width = matchParent, height = matchParent)

            listTeamFavorite = recyclerView {
                layoutManager = LinearLayoutManager(ctx)
            }.lparams(width = matchParent, height = matchParent)
        }
    }

    private fun showFavorite(){
        activity?.database?.use {
            val result = select(TeamFavorite.TABLE_TEAM_FAVORITE)
            val favorite = result.parseList(classParser<TeamFavorite>())
            teamFavorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }

}