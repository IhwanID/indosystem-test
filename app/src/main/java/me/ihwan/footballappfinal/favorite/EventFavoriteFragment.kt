package me.ihwan.footballappfinal.favorite

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.ihwan.footballappfinal.EventDetailActivity
import me.ihwan.footballappfinal.adapter.EventFavoriteAdapter
import me.ihwan.footballappfinal.database.database
import me.ihwan.footballappfinal.model.EventFavorite
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity


class EventFavoriteFragment : Fragment(), AnkoComponent<Context>{

    private var eventFavorites: MutableList<EventFavorite> = mutableListOf()
    private lateinit var adapter: EventFavoriteAdapter
    private lateinit var listEventFavorite: RecyclerView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = EventFavoriteAdapter(eventFavorites){
            startActivity<EventDetailActivity>("eventId" to it.eventId)
        }
        listEventFavorite.adapter = adapter

        showFavorite()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams(width = matchParent, height = matchParent)

            listEventFavorite = recyclerView {
                layoutManager = LinearLayoutManager(ctx)
            }.lparams(width = matchParent, height = matchParent)
        }
    }

    private fun showFavorite(){
        activity?.database?.use {
            val result = select(EventFavorite.TABLE_EVENT_FAVORITE)
            val favorite = result.parseList(classParser<EventFavorite>())
            eventFavorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }

}